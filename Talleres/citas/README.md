Vaccination Registration System
===
Abstract-Factory and Factory pattern.
## Table of Contents

1. [Beginners Guide](#beginners-guide)
   * [Requirements](#requirements)
   * [Steps](#steps)
   * [Instructions](#instructions)

2. [Intermediate Guide](#intermediate-guide)
   * [User Case Diagram](#user-case-diagram)

3. [Advanced Guide](#advanced-guide)
   * [Class Diagram](#class-diagram)
4. [Demo](#demo)
   * [Main](#main-menu)
   * [Make an appointment](#make-an-appointment)
   * [Search an appointment](#search-an-appointment)
   * [Exit](#exit)
   * [File System](#file-system)

5. [License](#license)

## Beginners Guide

If you are a total beginner to this, start here!

Requirements
---
* Java 11
* Maven
* IDE (IntelliJ, Eclipse, NetBeans, etc)

Steps
---
1. Is a maven project, so you need to import it to your IDE
2. Run the project with the main class: `com.example.Main`
3. The terminal will open, and you can start to use the system

Instructions
---
* The system will ask you to choose an option between 1 and 3
* If you choose 1, you will be able to make an appointment
* If you choose 2, you will be able to search an appointments
* If you choose 3, you will be able to exit the system
* If you choose a number that is not between 1 and 3, the system will ask you to choose again

## Intermediate Guide
### User Case Diagram
 * Case # 1: Make an appointment
![user flow](README-Resources/caseONE.svg)

```mermaid
    sequenceDiagram
        Alice->>System: Make me appointment to 2020-01-01 at 10:00
        Note right of System: Using the factory the system think if Alice can make it
        System-->>Alice: Yes, you can make it at 10:00 on 2020-01-01
        Note right of System: System convert to json the fabricate appointment
        System-->>Alice: Your appointment number is your DNI
        Note right of System: System now save the appointment in the database
        System-->>Alice: Your appointment is saved
        Note right of System: System send Alice to the main page
        System-->>Alice: See you!
```

        
* Case # 2: Search an appointments
![Case2](README-Resources/caseTWO.svg)

```mermaid
    sequenceDiagram
        Bob 👋 -->>System: Hello, I want to search appointments for 2020-01-01
		Note right of System: System reload database and search
		Note right of System: Using Fabric Method the system display the appointments
		System-->>Bob 👋: Here are the appointments for 2020-01-01
		Note right of Bob 👋: Thanks System. I Love you ❤️️ 
		Note right of System: System 👋 Bob to the main page
		System-->>Bob 👋: See you!
```

> Read more about sequence-diagrams here: http://bramp.github.io/js-sequence-diagrams/
 
Advanced Guide
---
### Class Diagram
[![Diagram Class](https://mermaid.ink/img/pako:eNqdVNtq4zAQ_RWhp11S_0CyFEqWLCm9PKTsk1-m0iRRa0tlJAdCN_9eyXIcOdJDWYORfebM0ZzR5ZMLI5HPuWjA2t8KdgRtrZl_1tohbUHgylDLfv2rqrtX6wiEC0DkpEhPYUvl4C-ITsPSXOmMZP8aOkb-8BPSmshPEVZVtwXJjPLnzRodY0ujHZkI5-JpNGpG_BGUjpNFQj2o9W2ZtuIzRhibCaMPSO7F3G-en378HHELB1zrlWpwc7QO23PolGqmrRslK7ZxpPSOSa0WGaihRZvDXs49lUOwwxyU4Aro3nSUo6GvA_rftgvm0yVlo_uRWOz91fYZs3y1gtB7CqKXcjwcin8xyyDjY1hchnSLJJKzsuTsO5LRWFrfxC4QwfFBWRcRe7bb64OU2Zw7dAGzd-fESfTDr1OMT2BCi1P4qq_nk5DWmTRjzbaXn0XCGapmh35cpNVXheqrxoBcmUYi-VnR2SyYZZQdVUVHFz_hBE96PvgTcVxMUvgNb5FaUNJffX1Wzd0eW6z53H9KoPea1_rkedA5szlqweeOOrzh3Uc4P8NNyedbaKxHUSrfrMfhLg3D6Qta6Ka_?type=png)](https://mermaid.live/edit#pako:eNqdVNtq4zAQ_RWhp11S_0CyFEqWLCm9PKTsk1-m0iRRa0tlJAdCN_9eyXIcOdJDWYORfebM0ZzR5ZMLI5HPuWjA2t8KdgRtrZl_1tohbUHgylDLfv2rqrtX6wiEC0DkpEhPYUvl4C-ITsPSXOmMZP8aOkb-8BPSmshPEVZVtwXJjPLnzRodY0ujHZkI5-JpNGpG_BGUjpNFQj2o9W2ZtuIzRhibCaMPSO7F3G-en378HHELB1zrlWpwc7QO23PolGqmrRslK7ZxpPSOSa0WGaihRZvDXs49lUOwwxyU4Aro3nSUo6GvA_rftgvm0yVlo_uRWOz91fYZs3y1gtB7CqKXcjwcin8xyyDjY1hchnSLJJKzsuTsO5LRWFrfxC4QwfFBWRcRe7bb64OU2Zw7dAGzd-fESfTDr1OMT2BCi1P4qq_nk5DWmTRjzbaXn0XCGapmh35cpNVXheqrxoBcmUYi-VnR2SyYZZQdVUVHFz_hBE96PvgTcVxMUvgNb5FaUNJffX1Wzd0eW6z53H9KoPea1_rkedA5szlqweeOOrzh3Uc4P8NNyedbaKxHUSrfrMfhLg3D6Qta6Ka_)
[![flowchart](https://mermaid.ink/img/pako:eNp9kl1vmzAUhv_Kka8JImwJhItKaUjURGsnjU6rBr2w8EmwBjYzpioF_nsNpK26SPOV9Z6v5z06LUklQxKQk6JlBvdhIsC8dXxLuXiE2eyq-4V5nsoCIVJ2B9ftJpOyQpAlKqq5FP1Ucj0kQxfxDjZxhFSl2eNFJIzXjF3K23j7zPVn_U6aYZOynZTtw_6-g3UiEjEzb4ptptjPiosT7GiqpWqgQJ1JZsM3SRkwqqkFqRRPqDRQwaA2-BRyeeLpGIUjzzWqDnbtWtC8ecF-GDK0331A3sQhr8qcNqAzBC6OUhXjAs7cN2dKwd7Bd5-svIGfycP_kf_AvzVWGgRWWFETGDkH-DcjWsIh-n4HHewvqPcf1Id36qG4ok84teJicI1QNZXG4mzhcGlh_4-FQSMWKdCY58wcTjtoCTE7KTAhgfkyqv4kJBG9yaO1llEjUhJoVaNF6tJMx5BTc28FCY40r4xaUkGCljyTYO469spbeXPH8f2vnr-wSEMCb27PvcXCXSx9z3f85aq3yIuUpoFje67J9r_43txdrlx3aRFk3Ozydrrr8bzHCb_HggGjfwUwnujd?type=png)](https://mermaid.live/edit#pako:eNp9kl1vmzAUhv_Kka8JImwJhItKaUjURGsnjU6rBr2w8EmwBjYzpioF_nsNpK26SPOV9Z6v5z06LUklQxKQk6JlBvdhIsC8dXxLuXiE2eyq-4V5nsoCIVJ2B9ftJpOyQpAlKqq5FP1Ucj0kQxfxDjZxhFSl2eNFJIzXjF3K23j7zPVn_U6aYZOynZTtw_6-g3UiEjEzb4ptptjPiosT7GiqpWqgQJ1JZsM3SRkwqqkFqRRPqDRQwaA2-BRyeeLpGIUjzzWqDnbtWtC8ecF-GDK0331A3sQhr8qcNqAzBC6OUhXjAs7cN2dKwd7Bd5-svIGfycP_kf_AvzVWGgRWWFETGDkH-DcjWsIh-n4HHewvqPcf1Id36qG4ok84teJicI1QNZXG4mzhcGlh_4-FQSMWKdCY58wcTjtoCTE7KTAhgfkyqv4kJBG9yaO1llEjUhJoVaNF6tJMx5BTc28FCY40r4xaUkGCljyTYO469spbeXPH8f2vnr-wSEMCb27PvcXCXSx9z3f85aq3yIuUpoFje67J9r_43txdrlx3aRFk3Ozydrrr8bzHCb_HggGjfwUwnujd)

> Read more about mermaid here: http://mermaid-js.github.io/mermaid/

Demo
---
 ### Main menu
![main menu](README-Resources/main.png)
    
### Make an appointment
![Make an appointment](README-Resources/add.png)

### Search an appointment
![Search an appointment](README-Resources/seaarch.png)

### Exit
![Exit](README-Resources/exit.png)

### File System
![file system](README-Resources/filesystem.png)
Los archivos json quedan guardados en la carpeta citas dentro del proyecto.

## License
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

###### tags: `Factory` `Learning`
