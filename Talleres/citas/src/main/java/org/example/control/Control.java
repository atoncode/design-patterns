package org.example.control;

import org.example.model.CitaVacunaCo;
import org.example.model.FactoryCitaI;
import org.example.model.Vacunas;

import java.io.File;
import java.io.IOException;

public class Control {
    FactoryCitaI factoryCita;
    Vacunas vacuna;

    public Control(){
       this.factoryCita = new FactoryCitaI();
       this.vacuna = new Vacunas(); // Deberia crear otra factiry para crear vacunas por que es para el driver-bussines
    }

    public void addCita(String dni, String names, String surnames, String age, String date,String hour){

        // Usar Factory para crear el objeto CitaVacunaCo
        CitaVacunaCo cita = this.factoryCita.createCita(dni, names, surnames, age, date, hour);
        cita.convertToJSON(); // Guardar en el sistema de archivos en formato JSON
    }

    public void loadFolderassets(File dir) {
        try {
            File[] files = dir.listFiles();
            assert files != null;
            for (File file : files) {
                if (file.isDirectory()) {
                    //System.out.println("directorio:" + file.getCanonicalPath());
                    loadFolderassets(file);
                } else {
                    //System.out.println("     archivo:" + file.getCanonicalPath());
                    // Convertir a objetos los archivos *.json
                    CitaVacunaCo jsonToClassCite = this.factoryCita.jsonToClassCite(file);
                    // Agregar a la lista de citas del sistema General para mostrar por terminal
                    this.vacuna.addCita(jsonToClassCite);

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void loadCita(){
        // Buscar en el sistema de archivos
        File miDir = new File("citas"); // directorio actual, cambiarlo si se quiere
        loadFolderassets(miDir);
    }

    public void printCitas(String date){
        this.vacuna.printCitas(date);
    }
    public CitaVacunaCo getCita(String dni) {
        loadCita();
        for (CitaVacunaCo cita : this.vacuna.getCitasArrayList()) {
            if (cita.getDni().equals(dni)) {
                //System.out.println(cita.getDni() + " " + cita.getNames() + " " + cita.getHour());
                return cita;
            }
        }

        return null;
    }

    public void resetCitas(){
        this.vacuna.resetCitas();
    }



}
