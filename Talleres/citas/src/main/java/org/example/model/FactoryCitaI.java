package org.example.model;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class FactoryCitaI implements IAbstractFactory {

        @Override
        public CitaVacunaCo createCita(String dni, String names, String surnames, String age,  String date, String hour) {
            return new CitaVacunaCo(dni, names, surnames, age, date, hour);
        }

        @Override
        public CitaVacunaCo jsonToClassCite(File file) throws FileNotFoundException {
            return new Gson().fromJson(new FileReader(file), CitaVacunaCo.class);
        }



}

