package org.example.model;

import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public abstract class Form implements IForm {

    protected String dni;
    protected String names;
    protected String lastNames;
    protected String age;
    protected String date;
    protected String hour;
    protected String json;

    public Form(String dni, String names, String lastNames, String age, String date, String hour) {
        this.dni = dni;
        this.names = names;
        this.lastNames = lastNames;
        this.age = age;
        this.date = date;
        this.hour = hour;
        this.json = null;
    }

    public String getDni() {
        return dni;
    }
    public String getDate() {
        return date;
    }
    public String getHour() {
        if(this.date != null){
            return this.hour;
        }
        return null;
    }
    public String getNames() {
        return names;
    }
    public String getAge() {
        return age;
    }

    @Override
    public void convertToJSON() {
        // Converter a JSON y guardar en el sistema de archivos
        Gson gson = new Gson();
        this.json = gson.toJson(this);

        try {
            JSONObject jsonObject = new JSONObject(this.json);
            System.out.println(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        saveInFileSystem();

    }
    @Override
    public void saveInFileSystem() {
        // Save file.json in file system
        // Guardar en el sistema de archivos el fichero .json organizado por dias
        // Crear un directorio por cada dia
        // Crear un fichero por cada hora
        // Crear un directorio por cada dia

        File dir = new File("citas/" + this.date);
        if(!dir.exists()){
            if(dir.mkdirs()){
                System.out.println("Directorio creado");
                System.out.println(dir.getAbsolutePath());
                // Guardar el fichero .json
                if(!Files.exists(Paths.get(dir.getAbsolutePath().concat("/").concat(this.dni.concat(".json"))))) {
                    try {
                        Files.createFile(Paths.get(dir.getAbsolutePath().concat("/").concat(this.dni.concat(".json"))));
                        Files.write(Paths.get(dir.getAbsolutePath().concat("/").concat(this.dni.concat(".json"))), this.json.getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else{
                    System.out.println("El fichero ya existe");
                }

            }else{
                System.out.println("Error al crear el directorio");
            }

        }else{
            //System.out.println(dir.getAbsolutePath());
            if(!Files.exists(Paths.get(dir.getAbsolutePath().concat("/").concat(this.dni.concat(".json"))))) {
                try {
                    Files.createFile(Paths.get(dir.getAbsolutePath().concat("/").concat(this.dni.concat(".json"))));
                    Files.write(Paths.get(dir.getAbsolutePath().concat("/").concat(this.dni.concat(".json"))), this.json.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else{
                System.out.println("El fichero ya existe");
            }
        }
    }

}
