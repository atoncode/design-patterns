package org.example.model;

import java.io.File;
import java.io.FileNotFoundException;

public interface IAbstractFactory {
      CitaVacunaCo createCita(String dni, String names, String surnames, String age,  String date, String hour);
      CitaVacunaCo jsonToClassCite(File file) throws FileNotFoundException;
}
