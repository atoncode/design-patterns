package org.example.model;

import java.util.ArrayList;

public class Vacunas {
    private final ArrayList<CitaVacunaCo> citasArrayList;

    public Vacunas() {
        this.citasArrayList = new ArrayList<>();
    }

    public void addCita(CitaVacunaCo cita) {
        this.citasArrayList.add(cita);
    }

    public ArrayList<CitaVacunaCo> getCitasArrayList() {
        return citasArrayList;
    }

    // print all the citas
    public void printCitas(String date) {
        for (CitaVacunaCo cita : citasArrayList) {
            if (cita.getDate().equals(date)) {
                System.out.println(cita.getDni()  + " " + cita.getNames() + " " + cita.getHour());
            }
        }
    }

    public void resetCitas() {
        this.citasArrayList.clear();
    }
}
