package org.example;

import org.example.control.Control;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Control control = new Control();
        int Option = prompt();
        while (Option != 3) {
            Scanner scanner = new Scanner(System.in);
            switch (Option) {
                case 1:
                    System.out.println("Add a new appointment");
                    System.out.print("Enter a Demografics Data: ");

                    System.out.print("Enter a DNI: ");
                    String dni = scanner.nextLine();

                    System.out.print("Enter a Name: ");
                    String names = scanner.nextLine();

                    System.out.print("Enter a Last Name: ");
                    String lastNames = scanner.nextLine();

                    System.out.print("Enter a Age: ");
                    String age = scanner.nextLine();

                    System.out.print("Enter a Date like 2022-02-01: ");
                    String date = scanner.nextLine();

                    System.out.print("Enter a Time like 10:00: ");
                    String time = scanner.nextLine();

                    // Revisar si la cita ya existe
                    control.loadCita();

                    if (control.getCita(dni) == null) {
                        control.addCita(dni, names, lastNames, age, date, time);
                        System.out.println("Appointment added");
                    } else {
                        System.out.println("Appointment already exists");
                    }

                    break;
                case 2:
                    control.resetCitas();
                    control.loadCita();
                    System.out.println("Search for an appointment");
                    System.out.print("Enter a Date like 2022-02-01: ");
                    String dateForSearch = scanner.nextLine();
                    control.printCitas(dateForSearch);
                    break;
                default:
                    System.out.println("Please enter a number between 1 and 3");
            }

            Option = prompt();
        }


    }

    // Simulate a terminal prompt
    public static int prompt() {
        do {
            System.out.println("1. Add a new appointment");
            System.out.println("2. Search for an appointment");
            System.out.println("3. Exit");

            System.out.print("Enter a number: ");
            Scanner scanner = new Scanner(System.in);
            try {
                return scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Please enter a number");
            }
        } while (true);


    }

}